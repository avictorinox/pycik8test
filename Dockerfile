FROM python:3.6
ENV PYTHONUNBUFFERED 1
ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8
RUN mkdir /webapps
WORKDIR /webapps
ADD . /webapps/

RUN apt-get update
RUN apt-get upgrade -y --no-install-recommends apt-utils


RUN apt-get install -y python3-pip

#RUN apt-get install -y vim

#RUN pip install --upgrade pip
RUN pip install -r /webapps/requirements.txt

#HEROKU FLASK
#CMD flask run --host=0.0.0.0 --port=4000