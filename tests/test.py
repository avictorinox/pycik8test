import unittest


def func(x):
    return x + 1


class MyTest(unittest.TestCase):

    def test_func_ok(self):
        print("fui testado 1")
        self.assertEqual(func(3), 4)

    def test_func_nok(self):
        print("fui testado 2")
        self.assertNotEqual(func(4), 4)


if __name__ == '__main__':
    unittest.main()